#ifndef IMAGE_H
#define IMAGE_H

#include <stdbool.h>

typedef struct tracker_matrix_type *Tracker_matrix;

Tracker_matrix create_tracker_matrix( int height, int width);
void destroy_tracker_matrix( Tracker_matrix tmat);
void load_tracker_matrix( Tracker_matrix tmat);
void set_tracker_cell_visited( Tracker_matrix tmat, int row, int col);
bool tracker_cell_visited( Tracker_matrix tmat, int row, int col);

#endif
