#include <stdio.h>		/* fprintf */
#include <stdlib.h>		/* exit */
#include "terminate.h"

void terminate( const char *message)
{
	fprintf( stderr, "%s\n", message);
	exit( EXIT_FAILURE);
}
