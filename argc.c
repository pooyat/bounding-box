/******
 * argc.c:  check number of arguments to white_areas program
 ******/

#include <stdio.h>              /* fprintf */
#include <stdlib.h>             /* exit */
#include "argc.h"
#include "terminate.h"

/*****
 * check_argc:  make sure two arguments are provided
 *****/
void check_argc( int arg_c)
{
	switch (arg_c) {
	case 1:
		terminate( "Two arguments are missing: file name and number of sections.\n");
		break;
	case 2:
		terminate( "Second argument is missing: number of sections.");
		break;
	case 3:
		break;
	default:
		terminate( "Too many arguments!\n");
	}
}
