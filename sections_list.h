#ifndef SECTIONS_LIST_H
#define SECTIONS_LIST_H

#include <stdbool.h>
#include "regions_list.h"

typedef struct section *Section;
typedef struct sections_list *Sections;

Section create_section( int height, int width);

void set_sec_x_min( Section sec, int x_mn);
void set_sec_y_min( Section sec, int y_mn);
void set_sec_x_max( Section sec, int x_mx);
void set_sec_y_max( Section sec, int y_mx);
void set_cell_visited( Section sec, int row, int col);

int get_sec_x_min( Section sec);
int get_sec_y_min( Section sec);
int get_sec_x_max( Section sec);
int get_sec_y_max( Section sec);
bool cell_visited( Section sec, int row, int col);

void add_rgn_to_sec( Section sec, Region rgn);
Regions get_rgns_list_of_sec( Section sec);

Sections create_sections_list();
void add_section( Sections sec_lst, Section sec);
Sections partition_image( int image_height, int image_width, double x);
void output_partition( Sections sec_lst);
void destroy_sections_list( Sections sec_lst);
void remove_current_section( Sections sec_lst);

Section get_first_sec( Sections sec_lst);
Section get_next_sec( Sections sec_lst);
bool end_of_sec_list( Sections sec_lst);
int get_sec_count( Sections sec_lst);

#endif
