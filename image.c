#include <stdio.h>              /* fprintf */
#include <stdlib.h>             /* malloc, EXIT_FAILURE */
#include "image.h"
#include "regions_list.h"
#include "sections_list.h"
#include "terminate.h"
#include "constants.h"

typedef char Pixel;

struct image_type {
	Pixel *framed_matrix;       /* framed matrix of ones and zeros */
	int framed_height;          /* framed column count */
	int framed_width;           /* framed row count */
	
	Regions regions_list;       /* a regions list */
};

/******
 * create_image: Serve the purpose of the default constructor in C++, but it
 * won't be invoked by default! It has to be called by the programmer whenever a
 * new Image object is declared.
 ******/
Image create_image( int height, int width)
{
	if (height < 0 || width < 0)
		terminate( "Negative height or width!");
	int frmd_height = height + 2 * PROX_RANGE,
		frmd_width = width + 2 * PROX_RANGE,
		size = frmd_height * frmd_width;
	Image img = malloc( sizeof( struct image_type));
	if (img == NULL)
		terminate( "Error in create_image:  image could not be created.");
	img->framed_matrix = malloc( size * sizeof( Pixel));
	if (img->framed_matrix == NULL)
		terminate( "Error in create_image:  image could not be created.");
	img->framed_height = frmd_height;
	img->framed_width = frmd_width;
	img->regions_list = create_regions_list();
	return img;
}

void destroy_image( Image img)
{
    free( img->framed_matrix);
    destroy_regions_list( img->regions_list);
	free( img);
}

/******
 * load_image: Load image from file stream to Image object.  Mark all pixels of
 * image as unvisited. Works only when Pixel is a char. So be ware if you ever
 * want to change the definition of Pixel.
 ******/
void load_image( Image img, FILE *txt_strm)
{
    int row = 0, col = 0,
		cell;                   /* getc returns an int */
	Pixel ch;                   /* works only when Pixel is a char */

	for (; row < PROX_RANGE; ++row) {
		/* add a row of black (0) cells at top of matrix */
		for (col = 0; col < img->framed_width; ++col) {
			img->framed_matrix[(row * img->framed_width) + col] = '0';
			/* img->framed_tracker_matrix[(row * img->framed_width) + col] = 'u'; */
		}
	}

	/* load cells after top row and before bottom row */
	/* for every row */
	/*     add a black cell at beginning of row */
	/*     for every column */
	/*         read in cell content */
	/*     add a black cell at end of row */
	for (; row < img->framed_height - PROX_RANGE; ++row) {
		for (col = 0; col < PROX_RANGE; ++col) { /* col must start from 0 here again */
			img->framed_matrix[(row * img->framed_width) + col] = '0';
			/* img->framed_tracker_matrix[(row * img->framed_width) + col] = 'u'; */
		}
		for (; col < img->framed_width - PROX_RANGE; ++col) {
			cell = getc( txt_strm);
			if (cell == EOF)
				terminate( "Pixels do not match dimensions.\n");
			ch = cell;
			if (ch == '\n' || ch == ' ') {	/* skip newline characters even if they are */
				--col;          /* spouted randomly among pixels */
				continue;
			}
			if (ch != '0' && ch != '1')
				fprintf( stderr, "Pixel other than 0 or 1 found: %c\n", ch), exit( EXIT_FAILURE);
			img->framed_matrix[(row * img->framed_width) + col] = ch;
			/* img->framed_tracker_matrix[(row * img->framed_width) + col] = 'u'; */
		}
		for (; col < img->framed_width; ++col) {
			img->framed_matrix[(row * img->framed_width) + col] = '0';
			/* img->framed_tracker_matrix[(row * img->framed_width) + col] = 'u'; */
		}
	}

	for (; row < img->framed_height; ++row) {
		/* add a row of black (0) cells at bottom of matrix */
		for (col = 0; col < img->framed_width; ++col) {
			img->framed_matrix[(row * img->framed_width) + col] = '0';
			/* img->framed_tracker_matrix[(row * img->framed_width) + col] = 'u'; */
		}
	}
}

/******
 * draw_box: Cells that form the box must be marked as visited because we don't
 * want the draw_boxeS function to find these cells as part of new regions.
 ******/
void draw_box( Image img, Region rgn)
{
	/* Region rgn = get_current_region( img->regions_list); */
	int x_mn = get_rgn_x_min( rgn);  int y_mn = get_rgn_y_min( rgn);
	int x_mx = get_rgn_x_max( rgn);  int y_mx = get_rgn_y_max( rgn);
	int col = x_mn;
	int row = y_mn;
	for (; col < x_mx; ++col)
		img->framed_matrix[(row * img->framed_width) + col] = '1';
	for (; row < y_mx; ++row)
		img->framed_matrix[(row * img->framed_width) + col] = '1';
	for (; x_mn < col; --col)
		img->framed_matrix[(row * img->framed_width) + col] = '1';
	for (; y_mn < row; --row)
		img->framed_matrix[(row * img->framed_width) + col] = '1';
}

void add_rgn_to_img(Image img, Region rgn)
{
	add_region( img->regions_list, rgn);
}

Regions get_rgns_list_of_img( Image img)
{
	return img->regions_list;
}

char get_pixel( Image img, int row, int col)
{
	return img->framed_matrix[(row * img->framed_width) + col];
}

/******
 * output_pbm: Output image to a pbm file
 ******/
void output_pbm( Image img, FILE *out_stream)
{
	fprintf( out_stream, "P1\n");
	fprintf( out_stream, "# Created by bounding_box program -- Pooya Taherkhani\n");
	fprintf( out_stream, "%d %d\n", img->framed_width - 2 * PROX_RANGE, img->framed_height - 2 * PROX_RANGE);

	Pixel *ch = img->framed_matrix;
	for (int row = PROX_RANGE; row < img->framed_height - PROX_RANGE; ++row) {
		for (int col = PROX_RANGE; col < img->framed_width - PROX_RANGE; ++col) {
			putc( ch[row * img->framed_width + col], out_stream);
		}
		putc( '\n', out_stream);
	}
}

void output_regions_count( Image img)
{
	/* printf( "%d", get_regions_count( img->regions_list)); */

	fprintf( stderr, "%d", get_regions_count( img->regions_list));
}

void output_image( Image img)
{
	Pixel *ch = img->framed_matrix;
	for (int row = PROX_RANGE; row < img->framed_height - PROX_RANGE; ++row) {
		for (int col = PROX_RANGE; col < img->framed_width - PROX_RANGE; ++col) {
			putchar( ch[row * img->framed_width + col]);
		}
		putchar('\n');
	}
}

void output_framed_image( Image img)
{
	Pixel *ch = img->framed_matrix;
	for (int row = 0; row < img->framed_height; ++row) {
		for (int col = 0; col < img->framed_width; ++col) {
			putchar( ch[row * img->framed_width + col]);
		}
		putchar('\n');
	}
}

void output_image_tracker( Image img)
{
	/* Pixel *ch = img->framed_tracker_matrix; */
	/* for (int row = PROX_RANGE; row < img->framed_height - PROX_RANGE; ++row) { */
	/* 	for (int col = PROX_RANGE; col < img->framed_width - PROX_RANGE; ++col) { */
	/* 		putchar( ch[row * img->framed_width + col]); */
	/* 	} */
	/* 	putchar('\n'); */
	/* } */
}

void output_framed_image_tracker( Image img)
{
	/* Pixel *ch = img->framed_tracker_matrix; */
	/* for (int row = 0; row < img->framed_height; ++row) { */
	/* 	for (int col = 0; col < img->framed_width; ++col) { */
	/* 		putchar( ch[row * img->framed_width + col]); */
	/* 	} */
	/* 	putchar('\n'); */
	/* } */
}
