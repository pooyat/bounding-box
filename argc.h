/******
 * argc.h:  check number of arguments to white_areas program
 ******/

#ifndef ARGC_H
#define ARGC_H

/*****
 * check_argc:  make sure tow arguments are provided
 *****/
void check_argc( int arg_c);

#endif
