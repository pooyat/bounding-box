#ifndef CONSTANTS_H
#define CONSTANTS_H

#define PROX_RANGE 3            /* region proximity range in pixels */
#define REGION_SIZE 100			/* miniumum region size in pixels */

#endif
