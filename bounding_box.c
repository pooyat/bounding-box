/*****
 * bounding_box.c:  Draw bounding boxes around characters in a given image
 *
 * This code is written in C.
 *
 * To compile and execute the program, issue the following at a linux shell's
 * command line:
 *
 * > make
 * > ./bounding_box <input_pbm_file> <x>
 *
 * Pooya Taherkhani
 * pt376511 @ ohio edu
 * March 2018
 *****/

#include <stdio.h>		/* FILE, printf */
#include <stdlib.h>		/* atoi, EXIT_SUCCESS, size_t */
#include <string.h>		/* strcat */
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <math.h>
#include "argc.h"
#include "file.h"
#include "image.h"
#include "sections_list.h"
#include "constants.h"
#include "terminate.h"

void* find_regions( void *img_sec_strct_ptr); /* thread function */
void traverse_region( Image img, Section sec, Region rgn, int row, int col);

struct img_sec_struct {
	Image img;
	Section sec;
};

int main( int argc, char *argv[])
{
	double x = atoi( argv[2]);
	FILE *in_stream, *out_stream;
	char *fname = argv[1];
	int height, width;
	/*int x_int = x; */ /* UNCOMMENT TO MEASURE EXECUTION TIME */

	check_argc( argc);
	open_file( &in_stream, fname);
	read_resolution( in_stream, &width, &height);

	Image img;
	img = create_image( height, width);
	load_image( img, in_stream);
	fclose( in_stream);

	/* UNCOMMENT TO MEASURE EXECUTION TIME */
	/* printf( "**************************\n"); */
	/* printf( "Number of threads: %d\n", (int) pow( 2, x_int)); */
	/* struct timeval tv1; */
	/* if (gettimeofday( &tv1, NULL) == -1) */
	/* 	terminate( "Error in gettimeofday."); */
	/* double timer1_milisec = 1000 * (long) tv1.tv_sec + 0.001 * (long) tv1.tv_usec; */

	Sections sec_list = partition_image( height, width, x);
	/* output_partition( sec_list); */

	struct img_sec_struct args[ get_sec_count( sec_list)];

	/* Launch threads */
	pthread_t tids[ get_sec_count( sec_list)];
	Section sec = get_first_sec( sec_list);
	int i = 0;
	while (!end_of_sec_list( sec_list)) {
		args[ i].img = img;
		args[ i].sec = sec;

		pthread_create( &tids[ i], NULL, find_regions, &args[ i]);

		sec = get_next_sec( sec_list);
		++i;
	}

	/* Wait until all threads are done */
	sec = get_first_sec( sec_list);
	i = 0;
	while (!end_of_sec_list( sec_list)) {
		pthread_join( tids[ i], NULL);
		
		sec = get_next_sec( sec_list);
		++i;
	}

	/* UNCOMMENT TO MEASURE EXECUTION TIME */
	/* struct timeval tv2; */
	/* if (gettimeofday( &tv2, NULL) == -1) */
	/* 	terminate( "Error in gettimeofday."); */
	/* double timer2_milisec = 1000 * (long) tv2.tv_sec + 0.001 * (long) tv2.tv_usec; */
	/* double run_time_2 = timer2_milisec - timer1_milisec; */
	/* printf( "run time: %.3f milisecs\n\n", run_time_2); */

	/* Loop over the sections and add regions of all sections to image regions
	 * list.  Do not add duplicate regions. */
	int duplicate_count = 0;
	Regions rgns_list = create_regions_list();
	Region rgn = create_region();
	sec = get_first_sec( sec_list);
	while (!end_of_sec_list( sec_list)) {
		rgns_list = get_rgns_list_of_sec( sec);
		rgn = get_first_rgn( rgns_list);
		while (!end_of_rgns_list( rgns_list)) {
			if (!duplicate_rgn( get_rgns_list_of_img( img), rgn))
				add_region( get_rgns_list_of_img( img), rgn);
			else
				++duplicate_count;
			rgn = get_next_rgn( rgns_list);
		}
		sec = get_next_sec( sec_list);
	}

	/* Draw_boxes */
	Region rgn2 = get_first_rgn( get_rgns_list_of_img( img));
	while (!end_of_rgns_list( get_rgns_list_of_img( img))) {
		draw_box( img, rgn2);
		rgn2 = get_next_rgn( get_rgns_list_of_img( img));
	}

	strcat( fname, ".new");
	create_file( &out_stream, fname);
	output_pbm( img, out_stream);
	fclose( out_stream);

	/* printf( "\nLook at file: %s\n\n", fname); */
	/* printf( "Matrix Resolution: %d by %d\n\n", height, width); */
	/* printf( "x = %d\n", x_int); */
	/* printf( "Total number of regions: "); */
	output_regions_count( img);
	/* printf( "\n"); */
	/* printf( "Number of extra region traversals: %d", duplicate_count); */
	/* printf( "\n\n"); */

	destroy_image( img);        /* (strive to) free up ALL allocated memory */

	return( EXIT_SUCCESS);
}

/******
 * find_regions: Find regions in a given section of the image (and add regions
 * to the regions list of that section).
 ******/
/* void find_regions( Image img, Section sec) { */
void* find_regions( void *img_sec_strct_ptr)
{
	/*
	 * Starting from the cell at (PROX_RANGE + 1)th row and (PROX_RANGE + 1)th
	 * column (and ending at the cell at (PROX_RANGE + 1) to the last row and
	 * (PROX_RANGE + 1) to the last column), scan cells row by row from left
	 * to right:
	 *     Upon hitting an unvisited black cell:
	 *     Create a Region (with default values, zero count)
	 *     Set y_max equal to the row of the hit cell
	 *     Add Region to Regions_list
	 *     traverse_region (find values of Region_data)
	 *     draw bounding box of the region that was traversed recently
	 *     (draw_box)
	 */
	struct img_sec_struct *img_sec_strct = img_sec_strct_ptr;

	int y_min = get_sec_y_min( img_sec_strct->sec);
	int y_max = get_sec_y_max( img_sec_strct->sec);
	int x_min = get_sec_x_min( img_sec_strct->sec);
	int x_max = get_sec_x_max( img_sec_strct->sec);
	for (int row = y_min + PROX_RANGE; row < y_max - PROX_RANGE; ++row) {
		for (int col = x_min + PROX_RANGE; col < x_max - PROX_RANGE; ++col) {
			if (get_pixel( img_sec_strct->img, row, col) == '1' &&
				!cell_visited( img_sec_strct->sec, row, col)) {
				Region new_region;
				new_region = create_region();
				set_rgn_x_min( new_region, col);  set_rgn_y_min( new_region, row);
				set_rgn_x_max( new_region, col);  set_rgn_y_max( new_region, row);
				traverse_region( img_sec_strct->img, img_sec_strct->sec, new_region, row, col);
				if (!small_region( new_region)) {
					add_rgn_to_sec( img_sec_strct->sec, new_region);
					/* output_region_props( new_region); */
				}
			}
		}
	}
	return NULL;
}

/******
 * traverse_region:  Traverse a region that contains the cell at (row, col)
 ******/
void traverse_region( Image img, Section sec, Region rgn, int row, int col)
{
	if (get_pixel( img, row, col) == '1' &&
		!cell_visited( sec, row, col) ) {
		set_cell_visited( sec, row, col);
		inc_pixel_count( rgn);
		update_box_coord( rgn, row, col);
		for (int i = -PROX_RANGE; i <= PROX_RANGE; ++i) {
			for (int j = -PROX_RANGE; j <= PROX_RANGE; ++j)
				traverse_region( img, sec, rgn, row + i, col + j);
		}
	}
}
