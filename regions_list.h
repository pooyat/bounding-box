#ifndef REGIONS_LIST_H
#define REGIONS_LIST_H

#include <stdbool.h>

typedef struct region *Region;

typedef struct regions_list *Regions;

Region create_region();

void set_pixel_count( Region rgn, int pixel_cnt);
void set_rgn_x_min( Region rgn, int x_mn);
void set_rgn_y_min( Region rgn, int y_mn);
void set_rgn_x_max( Region rgn, int x_mx);
void set_rgn_y_max( Region rgn, int y_mx);

int get_pixel_count( Region rgn);
int get_rgn_x_min( Region rgn);
int get_rgn_y_min( Region rgn);
int get_rgn_x_max( Region rgn);
int get_rgn_y_max( Region rgn);

void inc_pixel_count( Region rgn);
void update_box_coord( Region rgn, int row, int col);
bool small_region( Region rgn);
void output_region_props( Region rgn);

Regions create_regions_list();
void destroy_regions_list( Regions rgns_lst);
void add_region( Regions rgns_lst, Region rgn);
Region get_current_region( Regions rgns_lst); /* get most-recently added region */
int get_regions_count( Regions rgns_lst);
void remove_current_region( Regions rgns_lst);

Region get_first_rgn( Regions rgns_lst);
Region get_next_rgn( Regions rgns_lst);
bool end_of_rgns_list( Regions rgns_lst);

bool duplicate_rgn( Regions rgns_lst, Region rgn);

#endif
